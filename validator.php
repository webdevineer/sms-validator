function validateSMSContent($senderName, $message) 
{
	if(empty($senderName) AND empty($message)) :

		return array('status' => 'error', 'message' => 'No content posted.');

	endif;

	if(!empty($senderName)) :
	{
		$words = explode(" ", strtolower(trim($senderName)));

		$brandNames = file_get_contents(__DIR__ . '/../dependency/attributes/blacklisted_names.json');
		$forbiddenNames = json_decode($brandNames, true);
		foreach ($forbiddenNames as $brand) {
			if (in_array(strtolower($brand), $words)) {
				return array('status' => 'error', 'message' => 'Sender ID cannot contain brand/restricted names such as "<strong>' . $brand . '</strong>".');
			}
		}
	}
	endif;

	if(!empty($message)) :
	{
		$words = explode(" ", strtolower(trim($message)));

		$forbiddenWords = array("boko", "die", "terrorism", "violent", "threat", "danger", "attack", "harm", "weapon", "explosive", "kill", "murder", "bomb", "suicide", "extremist", "jihad", "assault", "hostage", "gun", "ammo", "riot", "revolt", "insurgency", "rebellion", "hijack", "kidnap", "seize", "ransom", "sabotage", "spy", "traitor", "treason", "subversive", "sedition", "anarchy", "coup", "militant", "faction", "radical", "rebel", "gang", "mafia", "cartel", "criminal", "smuggle", "traffick", "illegal", "corruption", "fraud", "theft", "bribe", "embezzle", "launder", "forgery", "counterfeit", "money-launder", "narcotic", "drug", "opiate", "cocaine", "heroin", "meth", "amphetamine", "cannabis", "marijuana", "methamphetamine", "psychoactive", "illegal-substance", "money-laundering", "phishing", "identity-theft", "cyber-crime", "hacking", "malware", "virus", "ransomware", "fraudulent", "spam", "scam", "scammer", "phisher", "scamming", "fake", "fraudster", "deceive", "impersonate", "spoof", "violence", "pirate", "piracy", "contraband", "cybercrime", "terrorist");
		foreach ($forbiddenWords as $word) {
			if (in_array(strtolower($word), $words)) {
				return array('status' => 'error', 'message' => 'Message cannot contain forbidden words such as "<strong>' . ucfirst($word) . '</strong>".');
			}
		}
	
		if (preg_match('/\b(sex|xxx|porn)\b/i', strtolower($message))) {
			return array('status' => 'error', 'message' => 'Message cannot contain explicit content.');
		}
	
		$forbiddenKeywords = array("Wow", "Promo", "Code", "Congrat", "Yellow", "Y'ello", "Vote");			
		foreach ($forbiddenKeywords as $keyword) {
			if (in_array(strtolower($keyword), $words)) {
				return array('status' => 'error', 'message' => 'Message cannot contain forbidden keywords such as "<strong>' . ucfirst($keyword) . '</strong>".');
			}
		}
	}
	endif;

	return array('status' => 'success', 'message' => 'Good to go!');
}